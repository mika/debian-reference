#!/bin/sh
# vim: set sts=4 expandtab:
bin/cnvtblcols.py < asciidoc/00_preface.txt  | sponge asciidoc/00_preface.txt  
bin/cnvtblcols.py < asciidoc/01_tutorial.txt | sponge asciidoc/01_tutorial.txt 
bin/cnvtblcols.py < asciidoc/02_package.txt  | sponge asciidoc/02_package.txt  
bin/cnvtblcols.py < asciidoc/03_sysinit.txt  | sponge asciidoc/03_sysinit.txt  
bin/cnvtblcols.py < asciidoc/04_auth.txt     | sponge asciidoc/04_auth.txt     
bin/cnvtblcols.py < asciidoc/05_network.txt  | sponge asciidoc/05_network.txt  
bin/cnvtblcols.py < asciidoc/06_netapp.txt   | sponge asciidoc/06_netapp.txt   
bin/cnvtblcols.py < asciidoc/07_xwindow.txt  | sponge asciidoc/07_xwindow.txt  
bin/cnvtblcols.py < asciidoc/08_i18nl10n.txt | sponge asciidoc/08_i18nl10n.txt 
bin/cnvtblcols.py < asciidoc/09_systips.txt  | sponge asciidoc/09_systips.txt  
bin/cnvtblcols.py < asciidoc/10_datamngt.txt | sponge asciidoc/10_datamngt.txt 
bin/cnvtblcols.py < asciidoc/11_dataconv.txt | sponge asciidoc/11_dataconv.txt 
bin/cnvtblcols.py < asciidoc/12_program.txt  | sponge asciidoc/12_program.txt  
bin/cnvtblcols.py < asciidoc/99_appendix.txt | sponge asciidoc/99_appendix.txt 
