== Preface

// vim: set sts=2 expandtab:

// Special sections starts with: "WARNING:" "CAUTION:" "NOTE:" "TIP:"

// Rebuild: Sun, 25 Mar 2012 11:51:53 +0000

This https://www.debian.org/doc/manuals/debian-reference/[Debian Reference (version @-@dr-version@-@)] (@-@build-date@-@) is intended to provide a broad overview of the Debian system administration as a post-installation user guide.

The target reader is someone who is willing to learn shell scripts but who is not ready to read all the C sources to figure out how the https://en.wikipedia.org/wiki/GNU[GNU]/https://en.wikipedia.org/wiki/Linux[Linux] system works.

For installation instructions, see:

- https://www.debian.org/releases/stable/installmanual[Debian GNU/Linux Installation Guide for current stable system]
- https://www.debian.org/releases/testing/installmanual[Debian GNU/Linux Installation Guide for current testing system]

=== Disclaimer

// The reason behind "Debian Reference" was : Debian may be KFreeBSD.
// It has many BSD and other contribution.
// This really document "Debian" things such as packaging and its network configuration middle layers.
// I as the lead author do not wish to make any political statement here either.
// Thus followings are commented out.
// For the sake of brevity, in this document Debian GNU/Linux is usually referred to as simply Debian.  ...

All warranties are disclaimed.  All trademarks are property of their respective trademark owners.

The Debian system itself is a moving target.  This makes its documentation difficult to be current and correct.  Although the current unstable version of the Debian system was used as the basis for writing this, some contents may be already outdated by the time you read this.

Please treat this document as the secondary reference. This document does not replace any authoritative guides. The author and contributors do not take responsibility for consequences of errors, omissions or ambiguity in this document.

=== What is Debian

The https://www.debian.org[Debian Project] is an association of individuals who have made common cause to create a free operating system.  It@@@sq@@@s distribution is characterized by the following.

- Commitment to the software freedom: https://www.debian.org/social_contract[Debian Social Contract and Debian Free Software Guidelines (DFSG)]
- Internet based distributed unpaid volunteer effort: https://www.debian.org
- Large number of pre-compiled high quality software packages
- Focus on stability and security with easy access to the security updates
- Focus on smooth upgrade to the latest software packages in the `unstable` and `testing` archives
- Large number of supported hardware architectures

Free Software pieces in Debian come from 
https://en.wikipedia.org/wiki/GNU[GNU], 
https://en.wikipedia.org/wiki/Linux[Linux], 
https://en.wikipedia.org/wiki/Berkeley_Software_Distribution[BSD], 
https://en.wikipedia.org/wiki/X_Window_System[X], 
https://en.wikipedia.org/wiki/Internet_Systems_Consortium[ISC], 
https://en.wikipedia.org/wiki/Apache_Software_Foundation[Apache], 
https://en.wikipedia.org/wiki/Ghostscript[Ghostscript], 
https://en.wikipedia.org/wiki/Common_Unix_Printing_System[Common Unix Printing System ], 
https://en.wikipedia.org/wiki/Samba_(software)[Samba], 
https://en.wikipedia.org/wiki/GNOME[GNOME], 
https://en.wikipedia.org/wiki/KDE[KDE], 
https://en.wikipedia.org/wiki/Mozilla[Mozilla], 
https://en.wikipedia.org/wiki/LibreOffice[LibreOffice], 
https://en.wikipedia.org/wiki/Vim_(text_editor)[Vim], 
https://en.wikipedia.org/wiki/TeX[TeX], 
https://en.wikipedia.org/wiki/LaTeX[LaTeX], 
https://en.wikipedia.org/wiki/DocBook[DocBook], 
https://en.wikipedia.org/wiki/Perl[Perl], 
https://en.wikipedia.org/wiki/Python_(programming_language)[Python], 
https://en.wikipedia.org/wiki/Tcl[Tcl], 
https://en.wikipedia.org/wiki/Java_(programming_language)[Java], 
https://en.wikipedia.org/wiki/Ruby_(programming_language)[Ruby], 
https://en.wikipedia.org/wiki/PHP[PHP], 
https://en.wikipedia.org/wiki/Berkeley_DB[Berkeley DB], 
https://en.wikipedia.org/wiki/MariaDB[MariaDB], 
https://en.wikipedia.org/wiki/PostgreSQL[PostgreSQL], 
https://en.wikipedia.org/wiki/Sqlite[SQLite], 
https://en.wikipedia.org/wiki/Exim[Exim], 
https://en.wikipedia.org/wiki/Postfix_(software)[Postfix], 
https://en.wikipedia.org/wiki/Mutt_(e-mail_client)[Mutt], 
https://en.wikipedia.org/wiki/FreeBSD[FreeBSD], 
https://en.wikipedia.org/wiki/OpenBSD[OpenBSD], 
https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs[Plan 9] 
and many more independent free software projects.  Debian integrates this diversity of Free Software into one system.

=== About this document

==== Guiding rules

Following guiding rules are followed while compiling this document.

- Provide overview and skip corner cases. (**Big Picture**)
- Keep It Short and Simple. (**KISS**)
- Do not reinvent the wheel. (Use pointers to **the existing references**)
- Focus on non-GUI tools and consoles. (Use **shell examples**)
- Be objective. (Use http://popcon.debian.org/[popcon] etc.)

TIP: I tried to elucidate hierarchical aspects and lower levels of the system.

// Although I do not list these, this is for my own reminder
// * Make it release independent.
// * Make as modular as possible for easier maintenance.
// * Use scripts and examples to make the key content language independent.

==== Prerequisites

WARNING: You are expected to make good efforts to seek answers by yourself beyond this documentation.  This document only gives efficient starting points.

You must seek solution by yourself from primary sources.

- https://www.debian.org/doc/manuals/debian-handbook/[The Debian Administrator's Handbook]
- The Debian site at https://www.debian.org for the general information
- The documentation under the "`/usr/share/doc/<package_name>`" directory
- The Unix style **manpage**: "`dpkg -L <package_name> |grep '/man/man.\*/'`"
- The GNU style **info page**: "`dpkg -L <package_name> |grep '/info/'`"
- The bug report: https://bugs.debian.org/[http://bugs.debian.org/<package_name>]
- The Debian Wiki at https://wiki.debian.org/ for the moving and specific topics
- The HOWTOs from The Linux Documentation Project (TLDP) at http://tldp.org/
- The Single UNIX Specification from the Open Group@@@sq@@@s The UNIX System Home Page at http://www.unix.org/
- The free encyclopedia from Wikipedia at https://www.wikipedia.org/

NOTE: For detailed documentation, you may need to install the corresponding documentation package named with "`-doc`" as its suffix.

==== Conventions

This document provides information through the following simplified presentation style with `bash`(1) shell command examples.

--------------------
# <command in root account>
$ <command in user account>
--------------------

// some actions are written in imperative for their simplicity

These shell prompts distinguish account used and correspond to set environment variables as: "`PS1='\$'`" and "`PS2=' '`".  These values are chosen for the sake of readability of this document and are not typical on actual installed system.

NOTE: See the meaning of the "`$PS1`" and "`$PS2`" environment variables in `bash`(1).

**Action** required by the system administrator is written in the imperative sentence, e.g. "Type Enter-key after typing each command string to the shell."

The **description** column and similar ones in the table may contain a **noun phrase** following https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices#bpp-desc-basics[the package short description convention] which drops leading articles such as "a" and "the".  They may alternatively contain an infinitive phrase as a **noun phrase** without leading "to" following the short command description convention in manpages.  These may look funny to some people but are my intentional choices of style to keep this documentation as simple as possible.  These **Noun phrases** do not capitalize their starting nor end with periods following these short description convention.

// This last capitalization rule is not compatible with IBM Style Tables in P234.

NOTE: Proper nouns including command names keeps their case irrespective of their location.

// translator of above paragraph are free to add their convention in their language in the same paragraph.

A **command snippet** quoted in a text paragraph is referred by the typewriter font between double quotation marks, such as "`aptitude safe-upgrade`".

A **text data** from a configuration file quoted in a text paragraph is referred by the typewriter font between double quotation marks, such as "`deb-src`".

A **command** is referred by its name in the typewriter font optionally followed by its manpage section number in parenthesis, such as `bash`(1).  You are encouraged to obtain information by typing the following.

--------------------
$ man 1 bash
--------------------

A **manpage** is referred by its name in the typewriter font followed by its manpage section number in parenthesis, such as `sources.list`(5).   You are encouraged to obtain information by typing the following.

--------------------
$ man 5 sources.list
--------------------

An **info page** is referred by its command snippet in the typewriter font between double quotation marks, such as "`info make`".  You are encouraged to obtain information by typing the following.

--------------------
$ info make
--------------------

A **filename** is referred by the typewriter font between double quotation marks, such as "`/etc/passwd`".  For configuration files, you are encouraged to obtain information by typing the following.

--------------------
$ sensible-pager "/etc/passwd"
--------------------

A **directory name** is referred by the typewriter font between double quotation marks, such as "`/etc/apt/`".  You are encouraged to explore its contents by typing the following.

--------------------
$ mc "/etc/apt/"
--------------------

A **package name** is referred by its name in the typewriter font, such as `vim`.  You are encouraged to obtain information by typing the following.

--------------------
$ dpkg -L vim
$ apt-cache show vim
$ aptitude show vim
--------------------

A **documentation** may indicate its location by the filename in the typewriter font between double quotation marks, such as "`/usr/share/doc/base-passwd/users-and-groups.txt.gz`" and "`/usr/share/doc/base-passwd/users-and-groups.html`"; or by its https://en.wikipedia.org/wiki/Uniform_Resource_Locator[URL], such as https://www.debian.org[https://www.debian.org].  You are encouraged to read the documentation by typing the following.

--------------------
$ zcat "/usr/share/doc/base-passwd/users-and-groups.txt.gz" | sensible-pager
$ sensible-browser "/usr/share/doc/base-passwd/users-and-groups.html"
$ sensible-browser "https://www.debian.org"
--------------------

An **environment variable** is referred by its name with leading "`$`" in the typewriter font between double quotation marks, such as "`$TERM`".  You are encouraged to obtain its current value by typing the following.

--------------------
$ echo "$TERM"
--------------------

==== The popcon

The http://popcon.debian.org/[popcon] data is presented as the objective measure for the popularity of each package.  It was downloaded on @-@pop-date@-@ and contains the total submission of @-@pop-submissions@-@ reports over @-@pop-packages@-@ binary packages and @-@pop-architectures@-@ architectures.

NOTE: Please note that the `@-@arch@-@` `unstable` archive contains only @-@all-packages@-@ packages currently.  The popcon data contains reports from many old system installations.

// If you check popcon data, you see some old version of Ubuntu distribution etc. sending data to Debian popcon system.

The popcon number preceded with "V:" for "votes" is calculated by "1000 \* (the popcon submissions for the package executed recently on the PC)/(the total popcon submissions)".

The popcon number preceded with "I:"  for "installs" is calculated by "1000 \* (the popcon submissions for the package installed on the PC)/(the total popcon submissions)".

NOTE: The popcon figures should not be considered as absolute measures of the importance of packages.  There are many factors which can skew statistics.  For example, some system participating popcon may have mounted directories such as "`/bin`" with "`noatime`" option for system performance improvement and effectively disabled "vote" from such system.

==== The package size

The package size data is also presented as the objective measure for each package.  It is based on the "`Installed-Size:`" reported by "`apt-cache show`" or "`aptitude show`" command (currently on `@-@arch@-@` architecture for the `unstable` release).  The reported size is in KiB (https://en.wikipedia.org/wiki/Kibibyte[Kibibyte] = unit for 1024 bytes).

NOTE: A package with a small numerical package size may indicate that the package in the `unstable` release is a dummy package which installs other packages with significant contents by the dependency.  The dummy package enables a smooth transition or split of the package.

NOTE: A package size followed by "(*)" indicates that the package in the `unstable` release is missing and the package size for the `experimental` release is used instead.

==== Bug reports on this document

Please file bug reports on the `debian-reference` package using `reportbug`(1) if you find any issues on this document. Please include correction suggestion by "`diff -u`" to the plain text version or to the source.

=== Reminders for new users

// * "Confucius says important Unix lessons are (1) Don't change the
// permissions, (2) backup your data, (3) keep it simple, stupid, (4) read the
// freaking manual, (5) don't mess with package management, (6) don't type
// anything you don't understand, (7) always have a boot floppy or CD, (8) read
// your log files, (9) the FIRST error is the one that counts, (10) don't leave
// your root shell until you TEST your changes, (11) don't be root when you
// don't have to be" --- The IRC bot of {{{#debian}}}}, <dpkg>
//
// Actually, that last comment is worthwhile, even if it is convoluted.  I'd
// leave it in, or break it up into smaller quotes.  -KO I changed it to
// 'Confucius says' rather than 'Confucius say' because the deliberate grammar
// error is unnecessary.

// OA: I rephrased as below with some additions.

Here are some reminders for new users:

- Backup your data
- Secure your password and security keys
- https://en.wikipedia.org/wiki/KISS_principle[KISS (keep it simple stupid)]
 * Don't over-engineer your system
- Read your log files
 * The *FIRST* error is the one that counts
- https://en.wikipedia.org/wiki/RTFM[RTFM (read the fine manual)]
- Search the Internet before asking questions
- Don't be root when you don't have to be
- Don't mess with the package management system
- Don't type anything you don't understand
- Don't change the file permissions (before the full security review)
- Don't leave your root shell until you *TEST* your changes
- Always have an alternative boot media (USB memory stick, CD, ...)

=== Some quotes for new users

Here are some interesting quotes from the Debian mailing list which may help enlighten new users.

- "This is Unix.  It gives you enough rope to hang yourself."  --- Miquel van Smoorenburg `<miquels at cistron.nl>`
- "Unix IS user friendly...  It@@@sq@@@s just selective about who its friends are." --- Tollef Fog Heen `<tollef at add.no>`

Wikipedia has article "https://en.wikipedia.org/wiki/Unix_philosophy[Unix philosophy]" which lists interesting quotes.

